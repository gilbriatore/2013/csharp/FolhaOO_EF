﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FolhaOO_EF.Model;
using FolhaOO_EF.Controller;
using FolhaOO_EF.Util;

namespace FolhaOO_EF.View
{
    public partial class frmLocalizarFuncionario : Form
    {
        frmCadFuncionarios f;
        frmCadFolha f1;
        
        public frmLocalizarFuncionario(Form Form)
        {
            InitializeComponent();
            if (Form.Name.ToString().Equals("frmCadFuncionarios"))
            {
                f = (frmCadFuncionarios)Form;
            }
            if (Form.Name.ToString().Equals("frmCadFolha"))
            {
                f1 = (frmCadFolha)Form;
            }
        }

        private void btnLocalizar_Click(object sender, EventArgs e)
        {
            grdFuncionarios.Rows.Clear();
            foreach(Funcionario x in FuncionarioController.ObterFuncionarios(txtNome.Text))
            {
                grdFuncionarios.Rows.Add(x.Id, x.Nome);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (f != null)
            {
                try
                {
                    Funcionario Funcionario = new Funcionario();
                    Funcionario = FuncionarioController.ObterFuncionario(grdFuncionarios.CurrentRow.Cells[0].Value.ToString(), "id");
                    f.txtId.Text = Funcionario.Id.ToString();
                    f.txtNome.Text = Funcionario.Nome;
                    f.mskCpf.Text = Funcionario.Cpf;
                    Close();
                }
                catch
                {
                    new DefaultException("Funcionário inválido", ExceptionType.Error);
                }
            }
            if (f1 != null)
            {
                try
                {
                    f1.txtIdFuncionario.Text = grdFuncionarios.CurrentRow.Cells[0].Value.ToString();
                    f1.txtNomeFuncionario.Text = grdFuncionarios.CurrentRow.Cells[1].Value.ToString();
                    Close();
                }
                catch
                {
                    new DefaultException("Funcionário inválido", ExceptionType.Error);
                }
            }
        }

        private void frmLocalizarFuncionario_Load(object sender, EventArgs e)
        {

        }
    }
}
