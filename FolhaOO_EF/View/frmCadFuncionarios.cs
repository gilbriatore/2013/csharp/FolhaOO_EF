﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FolhaOO_EF.Controller;
using FolhaOO_EF.Util;
using FolhaOO_EF.Model;

namespace FolhaOO_EF.View
{
    public partial class frmCadFuncionarios : Form
    {
        public frmCadFuncionarios()
        {
            InitializeComponent();
        }

        private void HabilitarComponentes()
        {
            grpDados.Enabled = true;
            btnOk.Enabled = true;
            btnCancelar.Enabled = true;
        }

        private void DesabilitarComponentes()
        {
            grpDados.Enabled = false;
            btnOk.Enabled = false;
            btnCancelar.Enabled = false;
        }

        private void MostrarFuncionario(Funcionario Funcionario)
        {
            txtId.Text = Funcionario.Id.ToString();
            txtNome.Text = Funcionario.Nome;
            mskCpf.Text = Funcionario.Cpf;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            int Id;
            if (txtId.Text.Equals(""))
                Id = 0;
            else
                Id = int.Parse(txtId.Text);
            Funcionario Funcionario = new Funcionario();
            Funcionario = FuncionarioController.Persistir(Id, txtNome.Text, mskCpf.Text);
            if (Funcionario != null)
            {
                DesabilitarComponentes();
                MostrarFuncionario(Funcionario);
            }

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Utilities.ClearFields(this);
            DesabilitarComponentes();
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            Utilities.ClearFields(this);
            HabilitarComponentes();
            txtNome.Focus();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (txtId.Text != "")
            {
                HabilitarComponentes();
                txtNome.Focus();
            }  
        }

        private void btnLocalizar_Click(object sender, EventArgs e)
        {
            frmLocalizarFuncionario Localizar = new frmLocalizarFuncionario(this);
            Localizar.ShowDialog();
        }
    
    }
}
