﻿namespace FolhaOO_EF.View
{
    partial class frmCadFolha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadFolha));
            this.grpDados = new System.Windows.Forms.GroupBox();
            this.mskValor = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.mskHoras = new System.Windows.Forms.MaskedTextBox();
            this.lblHoras = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.mskAno = new System.Windows.Forms.MaskedTextBox();
            this.btnLocalizarFuncionario = new System.Windows.Forms.Button();
            this.txtNomeFuncionario = new System.Windows.Forms.TextBox();
            this.mskMes = new System.Windows.Forms.MaskedTextBox();
            this.lblMesAno = new System.Windows.Forms.Label();
            this.txtIdFuncionario = new System.Windows.Forms.TextBox();
            this.lblFuncionario = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            this.btnNovo = new System.Windows.Forms.ToolStripButton();
            this.tbaAlunos = new System.Windows.Forms.ToolStrip();
            this.btnLocalizar = new System.Windows.Forms.ToolStripButton();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.grpDados.SuspendLayout();
            this.tbaAlunos.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDados
            // 
            this.grpDados.Controls.Add(this.mskValor);
            this.grpDados.Controls.Add(this.label2);
            this.grpDados.Controls.Add(this.mskHoras);
            this.grpDados.Controls.Add(this.lblHoras);
            this.grpDados.Controls.Add(this.label1);
            this.grpDados.Controls.Add(this.mskAno);
            this.grpDados.Controls.Add(this.btnLocalizarFuncionario);
            this.grpDados.Controls.Add(this.txtNomeFuncionario);
            this.grpDados.Controls.Add(this.mskMes);
            this.grpDados.Controls.Add(this.lblMesAno);
            this.grpDados.Controls.Add(this.txtIdFuncionario);
            this.grpDados.Controls.Add(this.lblFuncionario);
            this.grpDados.Controls.Add(this.txtId);
            this.grpDados.Controls.Add(this.lblId);
            this.grpDados.Enabled = false;
            this.grpDados.Location = new System.Drawing.Point(13, 37);
            this.grpDados.Name = "grpDados";
            this.grpDados.Size = new System.Drawing.Size(451, 175);
            this.grpDados.TabIndex = 0;
            this.grpDados.TabStop = false;
            this.grpDados.Text = "Dados do aluno";
            // 
            // mskValor
            // 
            this.mskValor.Location = new System.Drawing.Point(208, 104);
            this.mskValor.Name = "mskValor";
            this.mskValor.PromptChar = ' ';
            this.mskValor.Size = new System.Drawing.Size(96, 20);
            this.mskValor.TabIndex = 7;
            this.mskValor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mskValor.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.mskValor.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.mskValor_MaskInputRejected);
            this.mskValor.Leave += new System.EventHandler(this.mskValor_Leave);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(144, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 13;
            this.label2.Text = "Valor/Hora";
            // 
            // mskHoras
            // 
            this.mskHoras.Location = new System.Drawing.Point(76, 104);
            this.mskHoras.Name = "mskHoras";
            this.mskHoras.PromptChar = ' ';
            this.mskHoras.Size = new System.Drawing.Size(56, 20);
            this.mskHoras.TabIndex = 6;
            this.mskHoras.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mskHoras.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // lblHoras
            // 
            this.lblHoras.Location = new System.Drawing.Point(12, 108);
            this.lblHoras.Name = "lblHoras";
            this.lblHoras.Size = new System.Drawing.Size(100, 23);
            this.lblHoras.TabIndex = 11;
            this.lblHoras.Text = "Horas";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(130, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 23);
            this.label1.TabIndex = 10;
            this.label1.Text = "/";
            // 
            // mskAno
            // 
            this.mskAno.Location = new System.Drawing.Point(144, 76);
            this.mskAno.Name = "mskAno";
            this.mskAno.PromptChar = ' ';
            this.mskAno.Size = new System.Drawing.Size(56, 20);
            this.mskAno.TabIndex = 5;
            this.mskAno.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mskAno.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // btnLocalizarFuncionario
            // 
            this.btnLocalizarFuncionario.Location = new System.Drawing.Point(396, 48);
            this.btnLocalizarFuncionario.Name = "btnLocalizarFuncionario";
            this.btnLocalizarFuncionario.Size = new System.Drawing.Size(40, 23);
            this.btnLocalizarFuncionario.TabIndex = 3;
            this.btnLocalizarFuncionario.Text = "...";
            this.btnLocalizarFuncionario.UseVisualStyleBackColor = true;
            this.btnLocalizarFuncionario.Click += new System.EventHandler(this.btnLocalizarFuncionario_Click);
            // 
            // txtNomeFuncionario
            // 
            this.txtNomeFuncionario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNomeFuncionario.Location = new System.Drawing.Point(144, 48);
            this.txtNomeFuncionario.MaxLength = 60;
            this.txtNomeFuncionario.Name = "txtNomeFuncionario";
            this.txtNomeFuncionario.ReadOnly = true;
            this.txtNomeFuncionario.Size = new System.Drawing.Size(240, 20);
            this.txtNomeFuncionario.TabIndex = 2;
            // 
            // mskMes
            // 
            this.mskMes.Location = new System.Drawing.Point(76, 76);
            this.mskMes.Name = "mskMes";
            this.mskMes.PromptChar = ' ';
            this.mskMes.Size = new System.Drawing.Size(56, 20);
            this.mskMes.TabIndex = 4;
            this.mskMes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mskMes.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            // 
            // lblMesAno
            // 
            this.lblMesAno.Location = new System.Drawing.Point(12, 80);
            this.lblMesAno.Name = "lblMesAno";
            this.lblMesAno.Size = new System.Drawing.Size(100, 23);
            this.lblMesAno.TabIndex = 4;
            this.lblMesAno.Text = "Mês/Ano";
            // 
            // txtIdFuncionario
            // 
            this.txtIdFuncionario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtIdFuncionario.Location = new System.Drawing.Point(76, 48);
            this.txtIdFuncionario.MaxLength = 60;
            this.txtIdFuncionario.Name = "txtIdFuncionario";
            this.txtIdFuncionario.ReadOnly = true;
            this.txtIdFuncionario.Size = new System.Drawing.Size(56, 20);
            this.txtIdFuncionario.TabIndex = 1;
            this.txtIdFuncionario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFuncionario
            // 
            this.lblFuncionario.Location = new System.Drawing.Point(8, 52);
            this.lblFuncionario.Name = "lblFuncionario";
            this.lblFuncionario.Size = new System.Drawing.Size(100, 23);
            this.lblFuncionario.TabIndex = 2;
            this.lblFuncionario.Text = "Funcionário";
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(76, 20);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(100, 20);
            this.txtId.TabIndex = 0;
            this.txtId.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblId
            // 
            this.lblId.Location = new System.Drawing.Point(12, 24);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(100, 23);
            this.lblId.TabIndex = 0;
            this.lblId.Text = "ID";
            // 
            // btnNovo
            // 
            this.btnNovo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnNovo.Image = ((System.Drawing.Image)(resources.GetObject("btnNovo.Image")));
            this.btnNovo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(23, 22);
            this.btnNovo.Text = "toolStripButton1";
            this.btnNovo.ToolTipText = "Novo";
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // tbaAlunos
            // 
            this.tbaAlunos.BackColor = System.Drawing.Color.Silver;
            this.tbaAlunos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNovo,
            this.btnLocalizar});
            this.tbaAlunos.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tbaAlunos.Location = new System.Drawing.Point(0, 0);
            this.tbaAlunos.Name = "tbaAlunos";
            this.tbaAlunos.Size = new System.Drawing.Size(477, 25);
            this.tbaAlunos.TabIndex = 9;
            this.tbaAlunos.Text = "toolStrip1";
            // 
            // btnLocalizar
            // 
            this.btnLocalizar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLocalizar.Image = ((System.Drawing.Image)(resources.GetObject("btnLocalizar.Image")));
            this.btnLocalizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLocalizar.Name = "btnLocalizar";
            this.btnLocalizar.Size = new System.Drawing.Size(23, 22);
            this.btnLocalizar.Text = "toolStripButton1";
            this.btnLocalizar.ToolTipText = "Localizar";
            // 
            // btnOk
            // 
            this.btnOk.Enabled = false;
            this.btnOk.Location = new System.Drawing.Point(308, 224);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 28);
            this.btnOk.TabIndex = 8;
            this.btnOk.Text = "&OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Enabled = false;
            this.btnCancelar.Location = new System.Drawing.Point(392, 224);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 28);
            this.btnCancelar.TabIndex = 9;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // frmCadFolha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 268);
            this.Controls.Add(this.grpDados);
            this.Controls.Add(this.tbaAlunos);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancelar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCadFolha";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Folhas de Pagamento";
            this.grpDados.ResumeLayout(false);
            this.grpDados.PerformLayout();
            this.tbaAlunos.ResumeLayout(false);
            this.tbaAlunos.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.GroupBox grpDados;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.MaskedTextBox mskAno;
        public System.Windows.Forms.TextBox txtNomeFuncionario;
        public System.Windows.Forms.MaskedTextBox mskMes;
        private System.Windows.Forms.Label lblMesAno;
        public System.Windows.Forms.TextBox txtIdFuncionario;
        private System.Windows.Forms.Label lblFuncionario;
        public System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.ToolStripButton btnNovo;
        private System.Windows.Forms.ToolStrip tbaAlunos;
        private System.Windows.Forms.ToolStripButton btnLocalizar;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancelar;
        public System.Windows.Forms.MaskedTextBox mskValor;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.MaskedTextBox mskHoras;
        private System.Windows.Forms.Label lblHoras;
        public System.Windows.Forms.Button btnLocalizarFuncionario;
    }
}