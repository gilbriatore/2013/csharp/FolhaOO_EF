﻿namespace FolhaOO_EF.View
{
    partial class frmLocalizarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpDados = new System.Windows.Forms.GroupBox();
            this.btnLocalizar = new System.Windows.Forms.Button();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.grdFuncionarios = new System.Windows.Forms.DataGridView();
            this.grdId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdNome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.grpResultados = new System.Windows.Forms.GroupBox();
            this.grpDados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdFuncionarios)).BeginInit();
            this.grpResultados.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDados
            // 
            this.grpDados.Controls.Add(this.btnLocalizar);
            this.grpDados.Controls.Add(this.txtNome);
            this.grpDados.Controls.Add(this.lblNome);
            this.grpDados.Location = new System.Drawing.Point(11, 10);
            this.grpDados.Name = "grpDados";
            this.grpDados.Size = new System.Drawing.Size(492, 88);
            this.grpDados.TabIndex = 4;
            this.grpDados.TabStop = false;
            this.grpDados.Text = "Dados da consulta";
            // 
            // btnLocalizar
            // 
            this.btnLocalizar.Location = new System.Drawing.Point(392, 40);
            this.btnLocalizar.Name = "btnLocalizar";
            this.btnLocalizar.Size = new System.Drawing.Size(91, 28);
            this.btnLocalizar.TabIndex = 6;
            this.btnLocalizar.Text = "&Localizar";
            this.btnLocalizar.UseVisualStyleBackColor = true;
            this.btnLocalizar.Click += new System.EventHandler(this.btnLocalizar_Click);
            // 
            // txtNome
            // 
            this.txtNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNome.Location = new System.Drawing.Point(12, 44);
            this.txtNome.MaxLength = 60;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(372, 20);
            this.txtNome.TabIndex = 5;
            // 
            // lblNome
            // 
            this.lblNome.Location = new System.Drawing.Point(8, 24);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(140, 23);
            this.lblNome.TabIndex = 4;
            this.lblNome.Text = "Informe uma parte do nome";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(307, 314);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(92, 28);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // grdFuncionarios
            // 
            this.grdFuncionarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdFuncionarios.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.grdId,
            this.grdNome});
            this.grdFuncionarios.Location = new System.Drawing.Point(8, 20);
            this.grdFuncionarios.MultiSelect = false;
            this.grdFuncionarios.Name = "grdFuncionarios";
            this.grdFuncionarios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdFuncionarios.Size = new System.Drawing.Size(472, 164);
            this.grdFuncionarios.TabIndex = 0;
            this.grdFuncionarios.TabStop = false;
            this.grdFuncionarios.DoubleClick += new System.EventHandler(this.btnOK_Click);
            // 
            // grdId
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.grdId.DefaultCellStyle = dataGridViewCellStyle4;
            this.grdId.HeaderText = "ID";
            this.grdId.Name = "grdId";
            this.grdId.ReadOnly = true;
            this.grdId.Width = 60;
            // 
            // grdNome
            // 
            this.grdNome.HeaderText = "Funcionário";
            this.grdNome.Name = "grdNome";
            this.grdNome.ReadOnly = true;
            this.grdNome.Width = 340;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(411, 314);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(92, 28);
            this.btnCancelar.TabIndex = 7;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // grpResultados
            // 
            this.grpResultados.Controls.Add(this.grdFuncionarios);
            this.grpResultados.Location = new System.Drawing.Point(11, 110);
            this.grpResultados.Name = "grpResultados";
            this.grpResultados.Size = new System.Drawing.Size(492, 196);
            this.grpResultados.TabIndex = 5;
            this.grpResultados.TabStop = false;
            this.grpResultados.Text = "Resultados da consulta";
            // 
            // frmLocalizarFuncionario
            // 
            this.AcceptButton = this.btnLocalizar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 356);
            this.Controls.Add(this.grpDados);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.grpResultados);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLocalizarFuncionario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Localizar Funcionários";
            this.Load += new System.EventHandler(this.frmLocalizarFuncionario_Load);
            this.grpDados.ResumeLayout(false);
            this.grpDados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdFuncionarios)).EndInit();
            this.grpResultados.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDados;
        private System.Windows.Forms.Button btnLocalizar;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.DataGridView grdFuncionarios;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdId;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdNome;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.GroupBox grpResultados;
    }
}