﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FolhaOO_EF.Util;
using FolhaOO_EF.Controller;
using FolhaOO_EF.Model;

namespace FolhaOO_EF.View
{
    public partial class frmCadFolha : Form
    {
        public frmCadFolha()
        {
            InitializeComponent();
        }

        private void MostrarFolha(Folha Folha)
        {
            txtId.Text = Folha.Id.ToString();
            txtIdFuncionario.Text = Folha.Funcionario.Id.ToString();
            txtNomeFuncionario.Text = Folha.Funcionario.Nome;
            mskMes.Text = Folha.Mes.ToString();
            mskAno.Text = Folha.Ano.ToString();
            mskValor.Text = Folha.Valor.ToString("N2");
        }

        private void HabilitarComponentes()
        {
            grpDados.Enabled = true;
            btnOk.Enabled = true;
            btnCancelar.Enabled = true;
        }

        private void DesabilitarComponentes()
        {
            grpDados.Enabled = false;
            btnOk.Enabled = false;
            btnCancelar.Enabled = false;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Folha Folha = new Folha();
            Folha = FolhaController.Persistir(txtIdFuncionario.Text, mskMes.Text, mskAno.Text, mskHoras.Text, mskValor.Text);
            if (Folha != null)
            {
                DesabilitarComponentes();
                MostrarFolha(Folha);
            }
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            Utilities.ClearFields(this);
            HabilitarComponentes();
            mskMes.Focus();
        }

        private void btnLocalizarFuncionario_Click(object sender, EventArgs e)
        {
            frmLocalizarFuncionario localizar = new frmLocalizarFuncionario(this);
            localizar.ShowDialog();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Utilities.ClearFields(this);
            DesabilitarComponentes();
        }

        private void mskValor_Leave(object sender, EventArgs e)
        {
            string valor = Utilities.ValidarMonetario(mskValor.Text);
            if (valor != null)
            {
                mskValor.Text = valor;
            }
            else
            {
                mskValor.Focus();
                mskValor.Text = "0";
            }
        }

        private void mskValor_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
