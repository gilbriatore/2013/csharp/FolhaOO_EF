﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FolhaOO_EF.Model;
using FolhaOO_EF.DAO;
using FolhaOO_EF.Negocio;
using FolhaOO_EF.Util;

namespace FolhaOO_EF.Controller
{
    class FuncionarioController
    {
        public static Funcionario ObterFuncionario(string Filtro, string Tipo)
        {
            switch(Tipo)
            {
                case "id":
                    return FuncionarioDAO.ObterFuncionarioPorId(new Funcionario(int.Parse(Filtro)));                    
                case "nome":
                    return FuncionarioDAO.ObterFuncionarioPorNome(new Funcionario(0, Filtro));                    
                case "cpf":
                    return FuncionarioDAO.ObterFuncionarioPorCpf(new Funcionario(Filtro));
                default:
                    return null;
            }            
        }
        public static IOrderedQueryable<Funcionario> ObterFuncionarios(string Filtro)
        {
            Funcionario Funcionario = new Funcionario(0, Filtro);
            return FuncionarioDAO.ObterFuncionariosPorNome(Funcionario);
        }
        public static Funcionario Persistir(int Id, string Nome, string Cpf)
        {
            if (Id == 0)
            {
                if (!Nome.Equals("") && !Cpf.Equals(""))
                {
                    Funcionario Funcionario = new Funcionario(Nome, Cpf);
                    if (FuncionarioNegocio.ValidarCpf(Funcionario))
                    {
                        if (FuncionarioDAO.ObterFuncionarioPorCpf(Funcionario) == null)
                        {
                            if (FuncionarioDAO.Incluir(Funcionario))
                            {
                                //Funcionario = FuncionarioDAO.ObterFuncionarioPorCpf(Funcionario);
                                return Funcionario;
                            }
                        }
                        else
                        {
                            new DefaultException("Funcionário já cadastrado", ExceptionType.Information);
                        }
                    }
                    else
                    {
                        new DefaultException("CPF inválido", ExceptionType.Alert);
                    }
                }
                else
                {
                    new DefaultException("Preecha os campos Nome e CPF", ExceptionType.Alert);
                }
            }
            else
            {
                if (!Nome.Equals(""))
                {
                    Funcionario Funcionario = new Funcionario(Cpf);
                    Funcionario = FuncionarioDAO.ObterFuncionarioPorCpf(Funcionario);
                    Funcionario.Nome = Nome;
                    if (FuncionarioDAO.Alterar(Funcionario))
                    {
                        Funcionario = FuncionarioDAO.ObterFuncionarioPorCpf(Funcionario);
                        return Funcionario;
                    }
                }
                else
                {
                    new DefaultException("Preecha o campo Nome", ExceptionType.Alert);
                }
            }
            return null;
        }        
    }
}
