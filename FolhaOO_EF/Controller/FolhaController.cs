﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FolhaOO_EF.Model;
using FolhaOO_EF.DAO;
using FolhaOO_EF.Util;

namespace FolhaOO_EF.Controller
{
    class FolhaController
    {
        public static Folha Persistir(string IdFuncionario, string Mes, string Ano, string Horas, string Valor)
        {
            try
            {
                if (IdFuncionario != "" && int.Parse(Mes) != 0 && int.Parse(Ano) != 0 && int.Parse(Horas) != 0 && float.Parse(Valor) != 0)
                {
                    Funcionario Funcionario = new Funcionario();
                    Funcionario = FuncionarioController.ObterFuncionario(IdFuncionario, "id");
                    Folha Folha = new Folha(Funcionario, int.Parse(Mes), int.Parse(Ano), int.Parse(Horas), float.Parse(Valor));
                    if (FolhaDAO.ObterFolha(Folha) == null)
                    {
                        if (FolhaDAO.Incluir(Folha))
                        {
                            Folha = FolhaDAO.ObterFolha(Folha);
                            return Folha;
                        }
                    }
                    else
                    {
                        new DefaultException("Folha de pagamento já cadastrada", ExceptionType.Information);
                    }
                }
                else
                {
                    new DefaultException("Todos os campos são de preenchimento obrigatório", ExceptionType.Alert);
                }
            }
            catch
            {
                new DefaultException("Erro ao incluir folha", ExceptionType.Error);
            }
            return null;
        }
    }
}
