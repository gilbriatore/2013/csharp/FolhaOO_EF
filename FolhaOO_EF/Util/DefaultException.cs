﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FolhaOO_EF.Util
{
    class DefaultException : System.Exception
    {
        public DefaultException() { }
        public DefaultException(string message, ExceptionType Type)
        {
            switch (Type)
            {
                case ExceptionType.Alert:
                    MessageBox.Show(message, "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                case ExceptionType.Error:
                    MessageBox.Show(message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case ExceptionType.Question:
                    MessageBox.Show(message, "Pergunta", MessageBoxButtons.OK, MessageBoxIcon.Question);
                    break;
                case ExceptionType.Information:
                    MessageBox.Show(message, "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;                
            }
        }
    }
}
