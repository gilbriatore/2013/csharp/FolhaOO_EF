﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace FolhaOO_EF.Util
{
    class Utilities
    {
        public static void ClearFields(Form Form)
        {
            foreach (Control c in Form.Controls)
            {
                if (c is GroupBox)
                {
                    foreach (Control x in ((GroupBox)(c)).Controls)
                    {
                        if (x is TextBox)
                        {
                            ((TextBox)(x)).Clear();
                        }
                        if (x is MaskedTextBox)
                        {
                            ((MaskedTextBox)(x)).Clear();
                        }
                    }
                }                
            }
        }

        public static string ValidarMonetario(string valor)
        {
            float x;
            try
            {
                x = float.Parse(valor);
                return x.ToString("N2");
            }
            catch
            {
                new DefaultException("Valor inválido.", ExceptionType.Error);
                return null;
            }
        }
    }
}
