﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaOO_EF.Model
{
    class Funcionario
    {
        public int Id { set; get; }
        public string Nome { set; get; }
        public string Cpf { set; get; }

        public Funcionario() { }
        public Funcionario(int Id, string Nome, string Cpf)
        {
            this.Id = Id;
            this.Nome = Nome;
            this.Cpf = Cpf;
        }
        public Funcionario(string Nome, string Cpf)
        {
            this.Nome = Nome;
            this.Cpf = Cpf;
        }
        public Funcionario(int Id)
        {
            this.Id = Id;
        }
        public Funcionario(int Id, string Nome)
        {
            this.Id = Id;
            this.Nome = Nome;
        }
        public Funcionario(string Cpf)
        {
            this.Cpf = Cpf;
        }
    }
}
