﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaOO_EF.Model
{
    class Folha
    {
        public int Id { set; get; }
        public virtual Funcionario Funcionario { set; get; }
        public int Mes {set; get;}
        public int Ano { set; get; }
        public int Horas { set; get; }
        public double Valor { set; get; }

        public Folha() { }

        public Folha(Funcionario Funcionario, int Mes, int Ano, int Horas, float Valor)
        {
            this.Funcionario = Funcionario;
            this.Mes = Mes;
            this.Ano = Ano;
            this.Horas = Horas;
            this.Valor = Valor;            
        }
    }
}
