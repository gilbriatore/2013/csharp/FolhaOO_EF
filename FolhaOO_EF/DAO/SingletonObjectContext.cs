﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FolhaOO_EF.DAO
{
    class SingletonObjectContext
    {
        private static readonly SingletonObjectContext instance = new SingletonObjectContext();
        private readonly FolhaEntities context;

        private SingletonObjectContext()
        {
            context = new FolhaEntities();
        }

        public static SingletonObjectContext Instance
        {
            get
            {
                return instance;
            }
        }

        public FolhaEntities Context
        {
            get
            {
                return context;
            }
        }
    }
}
