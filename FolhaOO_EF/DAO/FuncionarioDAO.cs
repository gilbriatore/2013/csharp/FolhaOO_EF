﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FolhaOO_EF.Model;
using System.Data.Entity;
using System.Data;
using FolhaOO_EF.Util;
namespace FolhaOO_EF.DAO
{
    class FuncionarioDAO
    {
        public static Funcionario ObterFuncionarioPorCpf(Funcionario Funcionario)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                Funcionario = db.Funcionarios.FirstOrDefault(x => x.Cpf.Equals(Funcionario.Cpf));
                return Funcionario;
            }
            catch
            {
                new DefaultException("Erro ao consultar funcionário.", ExceptionType.Error);
                return null;
            }
        }
        public static Funcionario ObterFuncionarioPorNome(Funcionario Funcionario)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                Funcionario = db.Funcionarios.FirstOrDefault(x => x.Nome.Equals(Funcionario.Nome));
                return Funcionario;
            }
            catch
            {
                new DefaultException("Erro ao consultar funcionário.", ExceptionType.Error);
                return null;
            }
        }
        public static Funcionario ObterFuncionarioPorId(Funcionario Funcionario)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                Funcionario = db.Funcionarios.FirstOrDefault(x => x.Id == Funcionario.Id);
                return Funcionario;
            }
            catch
            {
                new DefaultException("Erro ao consultar funcionário.", ExceptionType.Error);
                return null;
            }
        }
        public static bool Incluir(Funcionario Funcionario)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Funcionarios.Add(Funcionario);
                db.SaveChanges();
                return true;
            }
            catch
            {
                new DefaultException("Erro ao incluir o funcioário", ExceptionType.Error);
                return false;
            }
        }
        public static bool Alterar(Funcionario Funcionario)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Entry(Funcionario).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch
            {
                new DefaultException("Erro ao alterar o funcioário", ExceptionType.Error);
                return false;
            }
        }
        public static bool Excluir(Funcionario Funcionario)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Funcionarios.Remove(Funcionario);
                db.SaveChanges();
                return true;
            }
            catch
            {
                new DefaultException("Erro ao excluir o funcioário", ExceptionType.Error);
                return false; 
            }
        }
        public static IOrderedEnumerable<Funcionario> ObterFuncionarios()
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                IOrderedEnumerable<Funcionario> Funcionarios = db.Funcionarios.ToList().OrderBy(x => x.Nome);
                return Funcionarios;
            }
            catch
            {
                new DefaultException("Erro ao obter o funcioários", ExceptionType.Error);
                return null;  
            }
        }
        public static IOrderedQueryable<Funcionario> ObterFuncionariosPorNome(Funcionario Funcionario)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                IOrderedQueryable<Funcionario> Funcionarios = db.Funcionarios.Where(x => x.Nome.Contains(Funcionario.Nome)).OrderBy(x => x.Nome);
                return Funcionarios;
            }
            catch
            {
                new DefaultException("Erro ao obter funcionários", ExceptionType.Error);
                return null;
            }
        }
    }
}
