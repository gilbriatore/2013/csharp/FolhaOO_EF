﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FolhaOO_EF.Model;
using FolhaOO_EF.Util;
namespace FolhaOO_EF.DAO
{
    class FolhaDAO
    {
        public static bool Incluir(Folha Folha)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Folhas.Add(Folha);
                db.SaveChanges();
                return true;
            }
            catch
            {
                new DefaultException("Erro ao incluir folha de pagamento.", ExceptionType.Error);
                return false;
            }
        }

        public static Folha ObterFolha(Folha Folha)
        {
            FolhaEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                Folha = db.Folhas.FirstOrDefault(x => x.Funcionario.Id == Folha.Funcionario.Id && x.Mes == Folha.Mes && x.Ano == Folha.Ano);
                return Folha;
            }
            catch
            {
                new DefaultException("Erro ao obter folha de pagamento.", ExceptionType.Error);
                return null;
            }
        }

        public static IOrderedEnumerable<Folha> ObterFolhas(Folha Folha)
        {
            try
            {
                FolhaEntities db = SingletonObjectContext.Instance.Context;
                IOrderedEnumerable<Folha> Folhas = db.Folhas.Include("Funcionario").Where(x => x.Mes == Folha.Mes && x.Ano == Folha.Ano).ToList().OrderBy(x => x.Mes);
                return Folhas;
            }
            catch
            {
                new DefaultException("Erro ao obter folhas de pagamento", ExceptionType.Error);
                return null;
            }
        }
    }    
}
