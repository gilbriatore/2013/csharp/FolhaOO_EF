﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FolhaOO_EF.Model;
namespace FolhaOO_EF.Negocio
{
    class FolhaNegocio
    {
        public static float CalcularSalarioBruto(Folha folha)
        {
            return (float)(folha.Horas * folha.Valor);
        }

        public static float CalcularIR(Folha folha)
        {
            float bruto = CalcularSalarioBruto(folha);
            if (bruto <= 1372.81)
            {
                return 0;
            }
            else
            {
                if (bruto <= 2743.25)
                {
                    return (float)((bruto * .15) - 205.92);
                }
                else
                {
                    return (float)((bruto * .275) - 548.82);
                }
            }
        }

        public static float CalcularINSS(Folha folha)
        {
            float bruto = CalcularSalarioBruto(folha);
            if (bruto <= 868.29)
            {
                return (float)(bruto * 0.08);
            }
            else
            {
                if (bruto <= 1447.14)
                {
                    return (float)(bruto * 0.09);
                }
                else
                {
                    if (bruto <= 2894.28)
                    {
                        return (float)(bruto * 0.11);
                    }
                    else
                    {
                        return (float)318.37;
                    }
                }
            }
        }

        public static float CalcularFGTS(Folha folha)
        {
            float bruto = CalcularSalarioBruto(folha);
            return (float)(bruto * 0.08);
        }

        public static float CalcularSalarioLiquido(Folha folha)
        {
            return CalcularSalarioBruto(folha) - CalcularINSS(folha) - CalcularIR(folha);
        }
    }
}
